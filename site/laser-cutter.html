<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <title>Laser Cutter Information | Confluent</title>
        <link rel="icon" href="/images/favicon-32x32.png" sizes="32x32">
        <link rel="icon" href="/images/favicon-192x192.png" sizes="192x192">
        <link rel="apple-touch-icon-precomposed" href="/images/favicon-180x180.png">
        <meta name="msapplication-TileImage" content="/images/favicon-270x270.png">
        <meta name="msapplication-TileColor" content="#f99d33">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="The information and policy page regarding the laser cutter available at Confluent.">
        <meta property="og:site_name" content="Confluent">
        <meta property="og:title" content="Laser Cutter Information | Confluent">
        <meta property="og:type" content="website">
        <meta property="og:url" content="https://confluent.space/resources/laser-cutter">
        <meta property="og:image" content="https://confluent.space/images/pages/laser-cutter/links.jpg">
        <meta property="og:description" content="The information and policy page regarding the laser cutter available at Confluent.">
        <meta name="twitter:title" content="Laser Cutter Information | Confluent">
        <meta name="twitter:site" content="@confluent_space">
        <meta name="twitter:description" content="The information and policy page regarding the laser cutter available at Confluent.">
        <meta name="twitter:card" content="summary_large_image">
        <meta name="twitter:image" content="https://confluent.space/images/pages/laser-cutter/links.jpg">
        <script src="/scripts/menu.js"></script>
        <link rel="stylesheet" href="/styles/font-awesome.min.css">
        <link rel="stylesheet" href="/styles/main.css?1">
        <link rel="stylesheet" href="/styles/information.css">
        <link rel="stylesheet" href="/styles/laser-cutter.css">
    </head>
    <body>
        <header>
            <a href="/"><h1>Confluent</h1></a>
            <nav>
                <ul id="menu">
                    <li id="menu-toggle" class="smallscreens"><i class="fa fa-bars"><span>Menu</span></i></li>
                    <li class="smallscreens"><a href="/">Main</a></li>
                    <li><a href="/about-us">About</a></li>
                    <li><a href="/resources">Tools &amp; Spaces</a></li>
                    <li class="smallscreens subpage"><a href="/resources/large-format-cncs">Large Format Machines</a></li>
                    <li class="smallscreens subpage"><a href="/resources/laser-cutter">Laser Cutter</a></li>
                    <li class="smallscreens subpage"><a href="/resources/3d-printers">3D Printers</a></li>
                    <li><a href="/calendar">Events</a></li>
                    <!--<li><a href="https://store.confluent.space/shop">Store</a></li>-->
                </ul>
            </nav>
            <h2><span>Imagine</span> <span>Make</span> <span>Learn</span></h2>
        </header>
        <nav>
            <ul>
                <li>
                    <a href="#specifications">Specifications</a>
                    <ul>
                        <li><a href="#kerf">Kerf</a></li>
                        <li><a href="#files">Software and File Formats</a></li>
                    </ul>
                </li>
                <li>
                    <a href="#policy">Policy</a>
                    <ul>
                        <li><a href="#training">Training</a></li>
                        <li><a href="#usage">Usage</a></li>
                        <li><a href="#materials">Allowed and Forbidden Materials</a></li>
                        <li><a href="#thickness">Material Thickness</a></li>
                    </ul>
                </li>
                <li><a href="#rates">Laser (Time) Rates</a></li>
                <li><a href="#supplies">Purchasable Supplies</a></li>
                <li><a href="/resources/laser-cutter/tips">Tips &amp; Tricks</a></li>
            </ul>
        </nav>
        <div class="callout">
            <section id="specifications">
                <h3>Specifications</h3>
                <table>
                    <thead>
                        <tr>
                            <th>Property</th>
                            <th>Value</th>
                            <th>Notes</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Type</td>
                            <td>Epilog Mini 24</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Working area</td>
                            <td>24″ x 12″ (610 x 305 mm)</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Maximum Material Thickness</td>
                            <td>5.5"</td>
                            <td>¼″ is the thickest material cuttable; see the <a href="#thickness">policy section</a></td>
                        </tr>
                        <tr>
                            <td>Type of Laser</td>
                            <td>60 Watt CO<sub>2</sub> (<span class="_Tgc">λ</span> 10.6 microns)</td>
                            <td></td>
                        </tr>
                        <tr>
                            <td>Raster Image Resolution*</td>
                            <td>75 to 1,200 dpi</td>
                            <td>Does not apply to <a href="https://en.wikipedia.org/wiki/Vector_graphics">vector graphics</a></td>
                        </tr>
                        <tr>
                            <td>Repeatability</td>
                            <td>± .0005″ (.0127 mm)</td>
                            <td>Largest variation between two runs with the same starting point</td>
                        </tr>
                        <tr>
                            <td>Accuracy</td>
                            <td>± .01″ (.254 mm) over the entire table</td>
                            <td>Largest variation between expected and actual location of the laser head</td>
                        </tr>
                    </tbody>
                </table>
                <p class="footnote">* Raster images (graphics) are GIFs, JPGs, PNGs, and other <a href="https://en.wikipedia.org/wiki/Raster_graphics">pixel-based formats</a>.</p>
                <p>Some of these specs are shamelessly borrowed from <a href="https://www.epiloglaser.com/products/legend-laser-series.htm">their website</a>.</p>
                <h4 id="kerf">Kerf</h4>
                <p>Just like regular saws, cuts made with a laser have a thickness commonly known as “kerf”. The following values have been empirically determined on our laser cutter and can be used to adjust designs to achieve better sizing or fit if desired.</p>
                <table>
                    <thead>
                        <tr>
                            <th>Material</th>
                            <th>Kerf</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>⅛″ Baltic birch plywood</td>
                            <td>.15 mm (~.006")</td>
                        </tr>
                        <tr>
                            <td>⅛″ Acrylic</td>
                            <td>.2 mm (~.008")</td>
                        </tr>
                    </tbody>
                </table>
                <p>Please note kerf is also dependent on material thickness, the focal point, and the settings used. When the laser beam is focused to a point on the surface of a material, cuts will be tapered with the smallest kerf on top and the largest kerf on the bottom. If the focal point is manually set to be inside the material the cut will be an hourglass-shape with a similar kerf on top and bottom. This is because the laser beam spreads out before and after this focal point. This effect will be less noticeable as the material gets thinner. In addition, slower more powerful settings will burn away more material resulting in a larger kerf.</p>
                <h4 id="files">Software and File Formats</h4>
                <p>We use CorelDRAW X8 to interface with the laser cutter. Design finishing and creation can be done in CorelDRAW, but it is recommended you finish all major work in your software of choice before coming to the space. CorelDRAW accepts the following major formats: <a href="https://en.wikipedia.org/wiki/Adobe_Illustrator">AI</a>, <a href="https://en.wikipedia.org/wiki/CorelDRAW">CDR</a>, <a href="https://en.wikipedia.org/wiki/.dwg">DWG</a>, <a href="https://en.wikipedia.org/wiki/AutoCAD_DXF">DXF</a>, <a href="https://en.wikipedia.org/wiki/Encapsulated_PostScript">EPS</a>, PDF, <a href="https://en.wikipedia.org/wiki/SVG">SVG</a>, and common raster formats (JPG, PNG). For a more complete list, see <a href="https://support.corel.com/hc/en-us/articles/216435817-Supported-file-formats-for-CorelDRAW-Graphics-Suite-X5">the knowledge base page on CorelDRAW X5</a>. (We couldn't find one for X8, but they should be similar.)</p>
                <p>Do note that some formats, such as PDF, may be treated as if they were an image rather than a vector. Also, SVG may not always import correctly but <a href="https://inkscape.org/en/">Inkscape</a>, a free vector graphics design program, is installed on the laser cutter's computer and can be used to save the SVG to another format. If your SVG has layers you will likely lose them when doing this.</p>
                <p>You can use <a href="https://en.wikipedia.org/wiki/Computer-aided_design">CAD</a> programs, as opposed to vector or raster graphics programs, to design for the laser cutter. If you do, your best bet is to export to the DWG or DXF formats. Users of other programs should try exporting to EPS if their program's native format is not importable by CorelDRAW. Some raster images can be traced with Inkscape and turned into a vector if you need to make cuts instead of engraving but it may be hard to achieve the desired results.</p>
            </section>
        </div>
        <section>
            <h3 id="policy">Policy</h3>
            <p>This section reiterates the DOs, DON’Ts, and other policies related to using the laser cutter.</p>
            <h4 id="training">Training</h4>
            <p>Anyone who wants to use the laser cutter must be trained. No exceptions. Training will be held regularly and you can find the next training on our <a href="https://confluent.space/calendar/">calendar</a>. If for some reason there isn’t one scheduled and you’d like to be trained, drop us a line <a href="mailto:make@confluent.space">by email</a> or <a href="https://www.facebook.com/ConfluentSpace/">through Facebook</a>. Training covers safety, how to use the laser cutter software, procedures when using the laser cutter, and other general tips. It does not cover how to create files suitable for use with the laser cutter. Look for other classes on our calendar to cover that. You will also need to sign our <a href="/resources/documents#tools">laser cutter agreement</a> after taking training.</p>
            <h4 id="usage">Usage</h4>
            <p>You are expected to follow the rules outlined during training and to check in with the front desk before using the laser cutter. This is so the front desk can verify your training status and give you access to the laser computer. Failing to use the laser cutter properly and safely can result in being blocked from using it.</p>
            <p>If you would like to know if the laser cutter is currently in use, you can check the <a href="http://status.confluent.space">Confluent status page</a> for usage information on some of our tools.</p>
            <div id="materials">
                <div>
                    <h4>Allowed Materials</h4>
                    <p>Anything not on this list is forbidden. Contact Arthur McBain or leave a note to get in touch with the front desk if you’d like to discuss other materials. Please have a copy of the material’s <a href="https://en.wikipedia.org/wiki/Safety_data_sheet">MSDS</a> available.</p>
                    <ul>
                        <li>Acrylic (<a href="/resources/laser-cutter/materials#acrylic">where to buy</a>)</li>
                        <li>Delrin</li>
                        <li>“Battleship Gray” Linoleum** (<a href="/resources/laser-cutter/materials#linoleum">where to buy</a>)</li>
                        <li>Leather (<a href="/resources/laser-cutter/materials#leather">where to buy</a>)</li>
                        <li>Plywood, thin hardwoods, and veneer (<a href="/resources/laser-cutter/materials#wood">where to buy</a>)</li>
                        <li>Bamboo</li>
                        <li>Paper / Cardstock / Cardboard </li>
                        <li>Glass (<a href="/resources/laser-cutter/materials#glass">where to buy</a>)</li>
                        <li>Anodized Aluminum (<a href="/resources/laser-cutter/materials#aluminum">where to buy</a>)</li>
                        <li>Aluminum, Tin, Stainless Steel, or Brass with CerMark (<a href="/resources/laser-cutter/materials#metal">where to buy</a>)</li>
                        <li>Agate (<a href="/resources/laser-cutter/materials#agate">where to buy</a>)</li>
                        <li>Magnet Sheeting</li>
                        <li>[Black] Granite</li>
                        <li>Cardboard</li>
                    </ul>
                    <p class="footnote">** <strong>Warning:</strong> There are many modern floor coverings known as “linoleum” which are nothing like <a href="https://en.wikipedia.org/wiki/Linoleum">the original</a>. Lasering them is hazardous to your health and the machine. See the “<a href="/resources/laser-cutter/materials#linoleum">where to buy</a>” page to get the real thing.</p>
                </div>
                <div>
                    <h4>Forbidden Materials</h4>
                    <p>Anything not on the allowed list is to be considered forbidden unless Arthur McBain has otherwise given explicit permission on a case by case basis. This list is therefore not to be taken as complete or all-inclusive.</p>
                    <ul>
                        <li>Polycarbonate</li>
                        <li>PVC</li>
                        <li>HDPE (“milk jug plastic”)</li>
                        <li><a href="https://en.wikipedia.org/wiki/Polystyrene">Polystyrene foam</a> (“Styrofoam”)</li>
                        <li><a href="https://www.google.com/search?q=polypropylene+foam&amp;tbm=isch">Polypropylene Foam</a></li>
                        <li>Fiberglass</li>
                        <li>Carbon Fiber</li>
                        <li>ABS (ex: LEGO, some 3D printer plastic)</li>
                        <li>MDF</li>
                    </ul>
                    <p>Many of these are hazards. They may melt instead of cut, catch fire, or could also release toxic gases. It is in the best interest of your own health to follow these rules.</p>
                    <p><strong>DO NOT cut materials which are too thick. See the next section.</strong></p>
                </div>
                <h4 id="thickness">Material Thickness</h4>
                <p>The table below is the maximum nominal thickness allowed to be cut on the laser cutter for the listed materials. <strong>If your material is too thick, use one of the saws in the shop instead.</strong> Materials over these thicknesses can still be engraved.</p>
                <table>
                    <thead>
                        <tr>
                            <th>Material</th>
                            <th>Maximum Thickness</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Acrylic</td>
                            <td>¼″ (~6 mm)</td>
                        </tr>
                        <tr>
                            <td>Delrin</td>
                            <td>³⁄₁₆″ (~5 mm)</td>
                        </tr>
                        <tr>
                            <td>Plywood (Wood)</td>
                            <td>¼″ (~6 mm)</td>
                        </tr>
                        <tr>
                            <td>Bamboo</td>
                            <td>¼″ (~6 mm)</td>
                        </tr>
                        <tr>
                            <td>Glass</td>
                            <td>0″ (this machine can only etch and engrave glass)</td>
                        </tr>
                        <tr>
                            <td>Metal</td>
                            <td>0″ (this machine can only etch and engrave coated metals)</td>
                        </tr>
                    </tbody>
                </table>
                <p>If your material isn’t listed, ask first instead of just trying it. The harder a material is the thinner the maximum thickness will be.</p>
            </div>
        </section>
        <div class="callout">
            <section>
                <h3 id="rates">Laser (Time) Rates</h3>
                <p>These rates do not include materials. You must bring your own, subject to approval, or buy from Confluent. Failure to check in with the front desk after finishing with the laser cutter to pay for your use will result in a fine that must be paid before you can access the laser cutter again. Time is based on total usage of the laser cutter and the associated PC, i.e. the amount of time it could not be used by anyone else and not just time the machine spent on your jobs.</p>
                <table>
                    <thead>
                        <tr>
                            <th>Membership</th>
                            <th>Cost</th>
                        </tr>
                    </thead>
                    <tbody>
                    <!--<tr>
                            <td>Day Pass</td>
                            <td>$10 for first ½-hour, $5 for every 10 minutes thereafter</td>
                        </tr>
                        <tr>
                            <td>Standard Access Membership</td>
                            <td>Free</td>
                        </tr>-->
                        <tr>
                            <td>Extended Access Membership</td>
                            <td>Free</td>
                        </tr>
                    </tbody>
                </table>
            </section>
        </div>
        <section id="supplies">
            <h3>Purchasable Supplies</h3>
            <p>As a convenience Confluent has some supplies available for purchase. The items currently on offer and their prices are listed below. You can bring your own materials as long as they are on the approved list or have been previously approved.</p>
            <table>
                <thead>
                    <tr>
                        <th>Material</th>
                        <th>Size</th>
                        <th>Cost</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Baltic birch plywood</td>
                        <td>24″&nbsp;x 12″ (approx.)</td>
                        <td>$5</td>
                    </tr>
                    <tr>
                        <td>Acrylic</td>
                        <td>17″&nbsp;x 11″ (approx.)</td>
                        <td>$10</td>
                    </tr>
                    <tr>
                        <td>Acrylic</td>
                        <td>24″&nbsp;x 12″ (approx.)</td>
                        <td>$15</td>
                    </tr>
                </tbody>
            </table>
            <p>The current acrylic on hand varies in size, cost, and color. The values in the table are the maximums.</p>
        </section>
        <footer class="business-info">
            <div>
                <address>
                    Confluent<br>
                    285 Williams Blvd<br>
                    Richland, WA 99354
                </address>
                <dl>
                <!--<dt><i class="label fa fa-fw"><span>Hours:</span></i></dt>
                    <dd><strong>Wed-Thu: 5-8pm, Fri: 10-8pm</strong></dd>
                    <dt><i class="label fa fa-fw"><span>Hours:</span></i></dt>
                    <dd><strong>Sat-Sun: 10-5pm</strong></dd>-->
                    <dt><i class="label fa fa-fw"><span></span></i></dt>
                    <dd><strong>Open House: Sat 10-2pm</strong></dd>
                    <dt><i class="label fa fa-fw fa-phone"><span>Phone:</span></i></dt>
                    <dd>(509) 371-9267</dd>
                    <dt><i class="label fa fa-fw fa-envelope"><span>Email:</span></i></dt>
                    <dd><a href="mailto:confluent.space@gmail.com">confluent.space@gmail.com</a></dd>
                </dl>
                <ul>
                    <li><a href="https://www.facebook.com/ConfluentSpace"><i class="fa fa-facebook"><span>Facebook</span></i></a></li>
                    <li><a href="https://twitter.com/confluent_space"><i class="fa fa-twitter"><span>Twitter</span></i></a></li>
                    <li><a href="https://www.instagram.com/confluent_space/"><i class="fa fa-instagram"><span>Instagram</span></i></a></li>
                    <li><a href="https://www.flickr.com/photos/confluent_space/"><i class="fa fa-flickr"><span>Flickr</span></i></a></li>
                </ul>
                

            </div>
        </footer>
    </body>
</html>
